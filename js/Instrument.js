import { Note } from "./Note";

class Instrument
{

    constructor ( instrumentName )
    {
        this.instrumentName = instrumentName;
        this.notes = [];
    }

}

export { Instrument };