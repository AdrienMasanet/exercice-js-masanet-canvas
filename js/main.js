import { Sequencer } from "./Sequencer";
import { Instrument } from "./Instrument";
import { init, draw } from "./firstStep";

document.addEventListener( "DOMContentLoaded", function ()
{
    const instruments = [ new Instrument( "guitare" ), new Instrument( "basse" ), new Instrument( "piano" ), new Instrument( "violon" ), new Instrument( "batterie" ) ];

    const sequencer = new Sequencer( instruments, "rgba(0,100,100,1)" );
    sequencer.draw();
} );