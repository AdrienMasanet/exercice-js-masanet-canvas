import { Instrument } from "./Instrument";

function init ()
{
    const sequencer = document.querySelector( "#sequencer" );
    const sequencerContext = sequencer.getContext( "2d" );
    return ( sequencerContext );
}

function draw ( sequencerContext, instruments )
{
    sequencerContext.font = "20px serif";

    let yOffset = 150;
    let xOffset = 100;
    let notesCount = 35;

    // GENERATE UI ELEMENTS FOR EACH INSTRUMENT
    instruments.forEach( function ( instrument, index )
    {

        sequencerContext.fillText( instrument.instrumentName, 15, 30 + ( index * yOffset ) / 5, 100 );

        for ( let i = 0; i < notesCount; i++ )
        {
            sequencerContext.strokeStyle = "rgba(200,0,0,1)";
            sequencerContext.strokeRect( 100 + ( i * xOffset ) / 5, 15 + ( index * yOffset ) / 5, 15, 15 );
        }
    } );

    // NON-INSTRUMENT RELATED DECORATION
    sequencerContext.beginPath();
    sequencerContext.lineWidth = 4;
    sequencerContext.moveTo( 90, 14 );
    sequencerContext.lineTo( 90, 285 );
    sequencerContext.stroke();
}

export { init, draw };