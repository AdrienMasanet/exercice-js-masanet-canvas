import { Instrument } from "./Instrument";
import { Note } from "./Note";

class Sequencer
{
    constructor (instruments, sequencerColor )
    {
        this.sequencer = document.querySelector( "#sequencer" );
        this.sequencerContext = sequencer.getContext( "2d" );
        this.instruments=instruments;
        this.yOffset = 150;
        this.xOffset = 100;
        this.notesCount = 34;
        this.sequencerColor = sequencerColor;
        this.assignStyle();
    }

    draw ()
    {        
        // INSTRUMENT RELATED UI COMPONENTS 
        this.instruments.forEach( ( instrument, indexInstrument ) =>
        {

            this.sequencerContext.fillText( instrument.instrumentName, 15, 30 + ( indexInstrument * this.yOffset ) / 5, 100 );

            if(instrument.notes.length==0) {
                for(let i=0; i<=this.notesCount; i++) {
                    let note = new Note(this.sequencerContext,instrument,(100 + ( i * this.xOffset ) / 5),(15 + ( indexInstrument * this.yOffset ) / 5));
                }
            }

            instrument.notes.forEach( ( note, indexNote ) =>
            {
                instrument.notes[indexNote].draw(this.sequencerContext, indexNote);
            } );

            
            this.sequencerContext.beginPath();
            this.sequencerContext.lineWidth = 3;
            this.sequencerContext.moveTo( 90, (14 + ( indexInstrument * this.yOffset-35 ) / 5) );
            this.sequencerContext.lineTo( 90, (30 + ( indexInstrument * this.yOffset+35 ) / 5) );
            this.sequencerContext.stroke();
            this.sequencerContext.lineWidth = 1;

        } );

        // NON-INSTRUMENT RELATED UI COMPONENTS 
        /*this.sequencerContext.beginPath();
        this.sequencerContext.lineWidth = 4;
        this.sequencerContext.moveTo( 90, 14 );
        this.sequencerContext.lineTo( 90, 30 );
        this.sequencerContext.stroke();*/
    }

    assignStyle ()
    {
        this.sequencer.style.marginTop = "50px";
        this.sequencer.style.border = "solid 2px " + this.sequencerColor;
        this.sequencer.style.borderRadius = "5px";
        this.sequencer.style.boxShadow = "0 0 15px rgb(0 0 0 / 20%)";
        this.sequencerContext.font = "20px serif";
        this.sequencerContext.strokeStyle = this.sequencerColor;
    }

}

export { Sequencer };