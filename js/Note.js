class Note
{

    constructor ( sequencerContext, instrumentAffected, posX, posY )
    {
        this.sequencerContext = sequencerContext;
        this.instrumentAffected = instrumentAffected;
        this.posX = posX;
        this.posY = posY;
        instrumentAffected.notes.push(this);
    }

    draw(indexNote){
        this.sequencerContext.strokeRect(this.posX, this.posY , 15, 15 );
    }

}

export { Note };